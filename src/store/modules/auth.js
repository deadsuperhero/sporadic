//  store/modules/auth.js
import axios from 'axios'
const LOGIN_ENDPOINT = 'oauth/token'
var BASEURL
var CLIENT_ID
var CLIENT_SECRET

const state = {
  user: null,
  activities: null
}
const getters = {
  isAuthenticated: state => !!state.user,
  StateUser: state => state.user
}
const actions = {
  async LogIn ({commit}, User) {
    await axios({
      method: 'POST',
      url: `${BASEURL}/${LOGIN_ENDPOINT}`,
      data: {
        // Holy fuck, this is hacky.
        'username': User.get('username'),
        'password': User.get('password'),
        'grant_type': 'password',
        'client_id': `${CLIENT_ID}`,
        'client_secret': `${CLIENT_SECRET}`
      },
      headers: {
        'accept': 'application/json',
        'content-type': 'application/json'
      }
    })
      .then(resp => {
        const token = resp.data.access_token
        console.log(token)
        localStorage.setItem('token', token)
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
      })
    await commit('setUser', User.get('username'))
  },
  async LogOut ({commit}) {
    let user = null
    localStorage.removeItem('token')
    delete axios.defaults.headers.common['Authorization']
    commit('logout', user)
  }
}
const mutations = {
  setUser (state, username) {
    state.user = username
  },
  logout (state, user) {
    state.user = null
  }
}
export default {
  configure (cfg) {
    BASEURL = cfg.BASEURL || ''
    CLIENT_ID = cfg.CLIENT_ID
    CLIENT_SECRET = cfg.CLIENT_SECRET
  },
  state,
  getters,
  actions,
  mutations
}
