import axios from 'axios'
import store from '@/store'
const LISTS_ENDPOINT = 'api/v1/lists'
var BASEURL

export default {
  name: 'listsAPI',
  configure (cfg) {
    BASEURL = cfg.BASEURL || ''
  },
  createList (title) {
    return axios({
      method: 'POST',
      url: `${BASEURL}/${LISTS_ENDPOINT}/`,
      data: {
        'title': title,
        'replies_policy': 'list'
      }
    })
  },
  getList (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${LISTS_ENDPOINT}/` + id
    })
  },
  getLists () {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${LISTS_ENDPOINT}/`
    })
  },
  readList (id) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${LISTS_ENDPOINT}/` + id + '/accounts'
    })
  },
  appendList (id, accountIds) {
    return axios({
      method: 'POST',
      url: `${BASEURL}/${LISTS_ENDPOINT}/` + id + '/accounts',
      data: {
        'account_ids': [
          accountIds
        ]
      }
    })
  },
  updateList (id) {
    return axios({
      method: 'PUT',
      url: `${BASEURL}/${LISTS_ENDPOINT}/` + id
    })
  },
  deleteList (id) {
    return axios({
      method: 'DELTE',
      url: `${BASEURL}/${LISTS_ENDPOINT}/` + id
    })
  }
}
