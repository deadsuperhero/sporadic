import axios from 'axios'
import store from '@/store'
const TIMELINES_ENDPOINT = 'api/v1/timelines'
const HOME = 'api/v1/timelines/home'
var BASEURL

export default {
  name: 'timelineAPI',
  configure (cfg) {
    BASEURL = cfg.BASEURL || ''
  },
  publicIndex (params) {
    return axios.get(`${BASEURL}/${TIMELINES_ENDPOINT}` + '/public', {
      params: params
    })
  },
  homeStream (params) {
    return axios({
      method: 'GET',
      url: `${BASEURL}/${TIMELINES_ENDPOINT}` + '/home',
      params: params
    })
  }
}
